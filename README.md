# cookiecutter webapp template based on the below tech

## tech
- [ ] [Litestar](https://docs.litestar.dev/latest/index.html) >=2.9.0
- [ ] [Jinja](https://jinja.palletsprojects.com/en/3.1.x/templates/) pulled in via litestar
- [ ] [uvicorn] pulled in via litestar
- [ ] [htmx](https://htmx.org/docs/) v1.9.12
- [ ] [Alpinejs](https://alpinejs.dev/start-here) v3.14.0
- [ ] [Tailwincss](https://tailwindcss.com/docs/installation) v3.4.3
- [ ] [daisyUI](https://daisyui.com) v4.12.2

update pyproject.toml and package.json to update to newer versions is desred.

