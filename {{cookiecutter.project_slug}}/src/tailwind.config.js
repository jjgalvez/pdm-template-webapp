/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./{{cookiecutter.package_slug}}/**/*.{html,js}",

  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('daisyui'),
  ],
}

