import os
from pathlib import Path

from dotenv import load_dotenv

from litestar import (
    Litestar,
)

from litestar.template.config import TemplateConfig
from litestar.contrib.jinja import JinjaTemplateEngine
from litestar.static_files import create_static_files_router

from . import handlers

load_dotenv()

here = Path(__file__).resolve().parent
jinja_templates = here / 'templates_jinja'
static_files = here / 'static_files'

route_handlers = [
    create_static_files_router(path='/html', name='html', directories=[static_files]),
    create_static_files_router(path='/js', name='js', directories=[static_files / 'js']),
    create_static_files_router(path='/css', name='css', directories=[static_files / 'css']),
]
route_handlers.extend(handlers.routes)

app = Litestar(
    route_handlers=route_handlers,
    template_config=TemplateConfig(
        directory=jinja_templates,
        engine=JinjaTemplateEngine
    ),
    debug=True if os.getenv('{{cookiecutter.package_slug}}_DEBUG', False) else False
)
