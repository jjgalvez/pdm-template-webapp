import os
import uvicorn

def run() -> None:
    uvicorn.run(
        '{{cookiecutter.package_slug}}:app',
        reload=True if os.getenv('{{cookiecutter.package_slug}}_DEBUG', False) else False,
        )

if __name__ == "__main__":
    run()
