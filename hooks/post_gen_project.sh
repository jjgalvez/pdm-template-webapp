#!/bin/sh
ROOT="$PWD"
PACKAGE_ROOT="$ROOT/src/{{cookiecutter.package_slug}}"

echo "Installing Python dependancies in a venv"
pdm install

cd $PACKAGE_ROOT

echo "Installing Node dependancies"
npm install

cd $ROOT

echo "Creating .env file"
cat << EOF > .env
# place development environmental variables here
{{cookiecutter.package_slug}}_DEBUG="true"

EOF
